define(function(){

    var exports = function() {

        require(['workshop/ui/Foo'],function(Foo) {

            new Foo();

        });

    };

    return exports;

});