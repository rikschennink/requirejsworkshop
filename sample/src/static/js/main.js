require.config({
    baseUrl:'/sample/www/static/js/project/',
    paths:{
        'conditioner/tests':'../vendor/rikschennink/conditioner-tests',
        'conditioner':'../vendor/rikschennink/conditioner-0.10.0'
    }
});

require(['conditioner'],function(conditioner){

    conditioner.init({
        'modules':{
            'ui/Foo':{
                options:{
                    'foo':'bar'
                }
            }
        }
    });

});