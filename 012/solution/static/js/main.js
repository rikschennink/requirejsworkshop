require.config({
    baseUrl:'static/js/workshop/',
    paths:{
        'conditioner/tests':'../vendor/rikschennink/conditioner-tests',
        'conditioner':'../vendor/rikschennink/conditioner-0.10.0'
    }
});

require(['conditioner'],function(conditioner){

    conditioner.init({
        'modules':{
            'ui/Foo':{
                'options':{
                    'name':'doit'
                }
            }
        }
    });

});