define(['conditioner','conditioner/Observer'],function(conditioner,Observer){

    var exports = function(element) {

        var nodes = conditioner.getNodes('div',element);

        var syncedNodes = conditioner.sync(nodes);

        Observer.subscribe(syncedNodes,'load',function(){

            var barNode = conditioner.getNode('[data-module="ui/Baz"]',element);

            var response = barNode.execute('gimme');

            var fooNode = conditioner.getNode('[data-module="ui/Foo"]',element);
            fooNode.execute(response[0].result.response);

        });
    };

    return exports;

});