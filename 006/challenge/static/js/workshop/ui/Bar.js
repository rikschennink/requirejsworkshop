define(['require','module'],function(require,module){

    var exports = function() {

        var path = module.config().dynamicDependency;

        require([path],function(Foo) {

            new Foo();

        });

    };

    return exports;

});