define(function(){

    var exports = function Foo(element,options) {

        if (!options || !options.name) {
            throw new Error('No name or options supplied to Foo');
        }

        element.textContent = options.name;

        win('009');

    };

    exports.options = {
        name:null
    };

    return exports;

});