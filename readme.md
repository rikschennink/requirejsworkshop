# RequireJS Workshop

## Installation

Project setup!

* checkout the repository using `git`
* open terminal
* run `npm install`


Start ze servers!

* run `grunt workshop` to start the webserver
* open chrome and navigate to `localhost:4000`

Deploy ze missiles!

* run `grunt deploy` to run the jasmine tests and optimize the AMD modules