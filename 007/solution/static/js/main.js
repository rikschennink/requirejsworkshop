require.config({
    baseUrl:'static/js/workshop/',
    paths:{
        conditioner:'../vendor/rikschennink/conditioner-0.10.0'
    }
});

require(['conditioner'],function(conditioner){

    conditioner.init();

});