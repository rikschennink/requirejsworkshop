define(['conditioner','conditioner/Observer'],function(conditioner,Observer){

    var exports = function(element) {

        var node = conditioner.getNode('[data-module="ui/Foo"]',element);

        Observer.subscribe(node,'surrender',function(response){
            node.execute(response);
        });

        Observer.subscribe(node,'load',function(){
            node.execute('tickle');
        });

        if (node.areModulesActive()) {
            node.execute('tickle');
        }
    };

    return exports;

});