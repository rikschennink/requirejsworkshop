var win = (function() {

    return function(next) {

        var field = document.querySelector('.field');
        var nextBtn = document.createElement('a');
        nextBtn.className = 'next';
        nextBtn.href = '/' + next + '/challenge';
        nextBtn.style.display = 'none';
        field.querySelector('.platform').appendChild(nextBtn);
        field.classList.add('win');
        setTimeout(function(){
            nextBtn.style.display = 'block';
            field.classList.add('done');
        },1000);

    };

}());

document.addEventListener('DOMContentLoaded',function(){
    document.body.innerHTML += '<div class="field"><div class="platform"><span class="keen"></span><span class="zap"></span><span class="bloog"></span></div></div>';
});