module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),


        // Webserver
        connect:{
            server:{
                options:{
                    hostname:'*',
                    port:4000,
                    keepalive:true,
                    base:'./'
                }
            }
        },


        // Optimizer
        copy:{
            sample:{
                files:[
                    {
                        expand:true,
                        cwd:'sample/src',
                        src:'**',
                        dest:'sample/www'
                    }
                ]
            }
        },
        requirejs:{
            sample:{
                options:{

                    // https://github.com/jrburke/r.js/blob/master/build/example.build.js

                    // Location of app source (src)
                    appDir:'./sample/src/static/js/',

                    // Target location of app (dest)
                    dir:'./sample/www/static/js/',

                    // Use the sites main configuration file
                    mainConfigFile:'./sample/src/static/js/main.js',

                    // Set base path relative to appDir
                    baseUrl:'./project',

                    // Optimization mode
                    optimize:'uglify',

                    // Also merges text files
                    inlineText:true,

                    // Remove all comments
                    preserveLicenseComments:false,

                    // Core modules to merge
                    modules:[
                        {
                            name:'../main',
                            include:[

                                // ui modules to merge
                                'ui/Foo'
                                // etc.



                            ]
                        }
                    ]

                }
            }
        },


        // Jasmine
        jasmine:{
            sample:{

                // Modules to test
                src:[
                    './sample/src/static/js/project/**/*.js'
                ],
                options:{

                    // Where to find specs
                    specs:'./sample/spec/*Spec.js',

                    // Helpers (function bind not included in PhantomJS so causes problems)
                    helpers:['./sample/src/static/js/shim/Function.bind.js'],

                    // Just hangon to the runner, it's handy as it helps with debugging
                    keepRunner:true,

                    // RequireJS html template to use
                    template:require('grunt-template-jasmine-requirejs'),

                    // Configuration options for this template
                    templateOptions:{

                        // Use the main.js configuration file
                        requireConfigFile:'./sample/src/static/js/main.js',
                        requireConfig:{

                            // Setup base url
                            baseUrl:'./sample/src/static/js/project',

                            // We need conditioner for a lot of tests, so fetch it
                            deps:['conditioner']
                        }
                    }
                }
            }
        }


    });

    // Load tasks
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-jasmine');
    grunt.loadNpmTasks('grunt-contrib-requirejs');

    // Workshop mode
    grunt.registerTask('workshop',['connect']);

    // Optimize and Test code
    grunt.registerTask('deploy',['jasmine','copy','requirejs']);

};