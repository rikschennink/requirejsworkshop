define(['require'],function(require){

    var exports = function() {

        require(['./Foo'],function(Foo) {

            new Foo();

        });

    };

    return exports;

});